#print('hello world')
#python can multiply string what javaScript can't do javascript will return nan in this case
#print('*' * 10)
#print('/' * (20 - 10))
#print(' He is a not a bad person at all, ' * 5)
#price = 10
#rating = 4.4
#print(rating)
#is_true = True
#print(is_true)

#write a program for hospital
#name = 'Will smith'
#age = 45
#is_new = True

#personName = input(' What is your name ')
#personAge = input(' what is your age ')
#print(personName + ' is ' + personAge + ' years old ')
#birthYear = input('What is your Birth year? ')
#age = 2019 - int(birthYear)
#print(age)

#weightLbs = input('Enter your weight in lbs: ')
#weightKg = int(weightLbs) * 0.453592;
#print(weightKg)

#Multiple line string
#message = '''
#Hi jhon,
#How are you?
#i hope you are well
#'''
#print(message)

#Show the characters from the string
nature = 'what a nice environment'
#show the first character
#print(nature[0])
#show the second character
#print(nature[1])
#show the last index character
#print(nature[-1])
#show the next all characters of the specified index
#print(nature[0:])
#show the previous all characters of the specified index
#print(nature[:-1])
#show the previous all characters of the specified indexes
#print(nature[0:-1])